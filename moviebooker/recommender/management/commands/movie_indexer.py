import json
from django.core.management import BaseCommand

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

import string
import random


# import spacy
# from scispacy.abbreviation import AbbreviationDetector
from recommender.models import Movie


def preprocess(text):
    """
    nlp = spacy.load("en_core_sci_sm")
    nlp.add_pipe("abbreviation_detector")
    text = nlp(text)
    for abrv in doc._.abbreviations:
        #HATALI# text.index(abrv) = abrv._.long_form
    """

    ############3************************ABBREVIATIONS EXPAND EDILMEDI; EKSIK KALDI. ########################

    text = text.split()
    allwords = []
    for i in range(0, len(text)):
        tokens = word_tokenize(text[i])
        tokens = [w.lower() for w in tokens]
        table = str.maketrans('', '', string.punctuation)
        stripped = [w.translate(table) for w in tokens]
        words = [word for word in stripped if word.isalpha()]
        stop_words = set(stopwords.words("english"))
        stemmer = SnowballStemmer("english")
        words = [stemmer.stem(w) for w in words if not w in stop_words]
        allwords += words

    return allwords


def addtoindex(inv_index, genre):
    movienames = []
    for movie in Movie.objects.filter(category=genre):
        movieswords = preprocess(movie.desc)
        movienames.append((movie.title, len(movieswords)))  # len(movieswords) is the doc_len, REQUIRED FOR OKAPI
        movie.doc_len = len(movieswords)
        movie.save()
        for i in range(0, len(movieswords)):
            word = movieswords[i]
            if movieswords.index(word) < i:  # continue if this is not the first encounter of that word
                continue
            else:
                if word in inv_index:
                    inv_index[word].append([movie.id, movieswords.count(word)])  # [movie_no , term_frequency] is appended
                else:
                    inv_index[word] = [[movie.id, movieswords.count(word)]]  # in case a new word is added to the index

    vocabulary = list(inv_index.keys())
    return [vocabulary, movienames]


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for genre in ['horror', 'sport', 'sci_fi']:

            print(f'Started: {genre}')
            inverted_index = {}
            vocabulary = []
            movie_list = []

            returned_list = addtoindex(inverted_index, genre)
            vocabulary += returned_list[0]
            movie_list += returned_list[1]  # list of (moviename,doc_len) tuples

            with open(f'data/processed_data/invertedindex_{genre}', 'w') as file:
                file.write(str(inverted_index))

            with open(f'data/processed_data/vocab_{genre}', 'w') as file:
                file.write(str(vocabulary))

            with open(f'data/processed_data/movielist_{genre}', 'w') as file:
                file.write(str(movie_list))
            print(f'Finished: {genre}')
