from django.core.management.base import BaseCommand

import requests as re
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand

from recommender.models import Movie


class Command(BaseCommand):

    def handle(self, *args, **options):
        for movie in Movie.objects.filter(image_url__isnull=True):
            req = re.get(movie.url)
            content = req.text
            soup = BeautifulSoup(content, 'html.parser')
            d = soup.find('div', {'class': 'poster'})
            if not d:
                continue

            image = d.find('img')
            if not image:
                continue
            movie_img_url = image['src']
            print(movie.title, movie_img_url)
            movie.image_url = movie_img_url
            movie.save()
