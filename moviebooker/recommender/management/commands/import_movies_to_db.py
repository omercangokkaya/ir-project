import json

from django.core.management.base import BaseCommand

from recommender.models import Movie


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        Movie.objects.all().delete()

        movie_files = ['data/raw_data/movie_horror.json', 'data/raw_data/movie_sport.json', 'data/raw_data/movie_sci_fi.json']
        movie_bulk_data = []
        for file in movie_files:
            with open(file, 'r') as f:
                data = json.load(f)
                for movie in data:
                    movie_bulk_data.append(Movie(**movie))

        Movie.objects.bulk_create(movie_bulk_data)