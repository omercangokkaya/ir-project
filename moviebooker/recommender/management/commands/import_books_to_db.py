import json

from django.core.management.base import BaseCommand, CommandError

from recommender.models import Book


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        Book.objects.all().delete()

        book_files = ['data/raw_data/book_horror.json', 'data/raw_data/book_sport.json', 'data/raw_data/book_sci_fi.json']
        book_bulk_data = []
        for file in book_files:
            with open(file, 'r') as f:
                data = json.load(f)
                for movie in data:
                    book_bulk_data.append(Book(**movie))

        Book.objects.bulk_create(book_bulk_data)