import csv

import matplotlib.pyplot as plt
from django.core.management.base import BaseCommand

from recommender.models import Book, Movie
from recommender.utils import get_recommended_movies_by_book


def calculate_precision_recall_for_precision_recall_curve(recommended_movies, golden_set):
    recall_precision_list = []
    for i in range(1, len(recommended_movies) + 1):
        recommended_movies_tmp = set(recommended_movies[:i])
        golden_set_tmp = set(golden_set)

        common = recommended_movies_tmp.intersection(golden_set_tmp)
        recommended_movies_tmp -= common
        golden_set_tmp -= common

        tp = len(common)
        fp = len(recommended_movies_tmp)
        fn = len(golden_set_tmp)

        precision = tp / (tp + fp)
        recall = tp / (tp + fn)

        recall_precision_list.append([recall, precision])

    return recall_precision_list


class Command(BaseCommand):

    def handle(self, *args, **options):
        book_movie_golden_dict = {}
        with open('data/golden_set.csv', 'r') as f: # READ data from the golden set
            reader = list(csv.reader(f, delimiter=';'))
            for i in reader:
                if i:
                    book_id = i[0]
                    movie_id = i[2]
                    book = Book.objects.get(id=book_id) # Match the books objects with the data
                    book_movie_golden_dict.setdefault(book, []).append(int(movie_id))  # Group movies with respect to the books.

        y_true = []
        y_pred = []

        tp = 0
        fp = 0
        fn = 0

        rf_tp = 0
        rf_fp = 0
        rf_fn = 0
        total_recall_precision_list = [[] for i in range(10)]

        categorical_stats = {}
        for book, golden_set in book_movie_golden_dict.items():

            recommended_movies = [m.id for m in get_recommended_movies_by_book(book)] # Get recommended movies
            golden_set = set(golden_set) # get the golden set
            recall_precision_list = calculate_precision_recall_for_precision_recall_curve(recommended_movies, golden_set) # calculate precsion recall values for precision recall curve
            for i in range(10):
                total_recall_precision_list[i].append(recall_precision_list[i])

            recommended_movies = set(recommended_movies)

            common = recommended_movies.intersection(golden_set)
            recommended_movies -= common
            golden_set_tmp = golden_set - common

            # SET True Positive, False Positive and False Negative counts
            tp += len(common)
            fp += len(recommended_movies)
            fn += len(golden_set_tmp)

            # SET True Positive, False Positive and False Negative counts for each category
            if book.category in categorical_stats:
                categorical_stats[book.category]['tp'] += len(common)
                categorical_stats[book.category]['fp'] += len(recommended_movies)
                categorical_stats[book.category]['fn'] += len(golden_set_tmp)
            else:
                categorical_stats[book.category] = {}
                categorical_stats[book.category]['tp'] = len(common)
                categorical_stats[book.category]['fp'] = len(recommended_movies)
                categorical_stats[book.category]['fn'] = len(golden_set_tmp)

            # Get recommended movies with the feedback activated.
            recommended_movies = set([m.id for m in get_recommended_movies_by_book(book, feedback_movies=Movie.objects.filter(id__in=common))])
            common = recommended_movies.intersection(golden_set)
            recommended_movies -= common
            golden_set_tmp = golden_set - common

            # SET True Positive, False Positive and False Negative counts for relevance feedback
            rf_tp += len(common)
            rf_fp += len(recommended_movies)
            rf_fn += len(golden_set_tmp)

            for c in common:
                y_true.append(c)
                y_pred.append(c)

            golden_set = list(golden_set)
            recommended_movies = list(recommended_movies)
            i = 0
            for i in range(len(golden_set)):
                y_true.append(golden_set[i])
                if i < len(recommended_movies):
                    y_pred.append(recommended_movies[i])
                else:
                    y_pred.append(-1)

            if i <= len(recommended_movies):
                for i in range(i + 1, len(recommended_movies)):
                    y_true.append(-1)
                    y_pred.append(recommended_movies[i])

        # Calculate recall-precision values for the Precision Recall Curve
        recall_list = []
        precision_list = []
        for total_recall_precision in total_recall_precision_list:
            sum_recall = 0
            sum_precision = 0
            for recall, precision in total_recall_precision:
                sum_recall += recall
                sum_precision += precision
            recall = sum_recall / len(total_recall_precision)
            precision = sum_precision / len(total_recall_precision)
            recall_list.append(recall)
            precision_list.append(precision)

        plt.plot(recall_list, precision_list)

        # naming the x axis
        plt.xlabel('Recall axis')
        # naming the y axis
        plt.ylabel('Precision')

        # giving a title to my graph
        plt.title('Precision Recall Curve ')

        # function to show the plot
        plt.show()


        # Calculate precision recall f1 scores for overall system
        calculated_precision = tp / (tp + fp)
        calculated_recall = tp / (tp + fn)
        calculated_f1_score = 2 * tp / (2 * tp + fp + fn)

        print(f"Total Calculated Precision: {calculated_precision} Recall: {calculated_recall} F1: {calculated_f1_score}")


        # Calculate precision recall f1 scores for overall system for relevance feedback
        calculated_precision = rf_tp / (rf_tp + rf_fp)
        calculated_recall = rf_tp / (rf_tp + rf_fn)
        calculated_f1_score = 2 * rf_tp / (2 * rf_tp + rf_fp + rf_fn)

        print(f"Relevance Feedback Total Calculated Precision: {calculated_precision} Recall: {calculated_recall} F1: {calculated_f1_score}")

        # Calculate precision recall f1 scores for each category

        for category, stats in categorical_stats.items():
            calculated_precision = stats['tp'] / (stats['tp'] + stats['fp'])
            calculated_recall = stats['tp'] / (stats['tp'] + stats['fn'])
            calculated_f1_score = 2 * stats['tp'] / (2 * stats['tp'] + stats['fp'] + stats['fn'])
            print(f"Category: {category} Precision: {calculated_precision} Recall: {calculated_recall} F1: {calculated_f1_score}")
