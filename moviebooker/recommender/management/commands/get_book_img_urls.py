import json

from django.core.management.base import BaseCommand, CommandError

from recommender.models import Book
import requests as re
from bs4 import BeautifulSoup

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for book in Book.objects.filter(image_url__isnull=True):
            req = re.get(book.url)
            content = req.text
            soup = BeautifulSoup(content, 'html.parser')
            image = soup.find('img', {'id': 'coverImage'})
            if not image:
                continue
            book_title = image['alt']
            book_img_url = image['src']
            print(book.title, book_title, book_img_url )
            if 'isbn' in book.title.lower():
                print('title updated', book.title, book_title)
                book.title = book_title
            book.image_url = book_img_url
            book.save()