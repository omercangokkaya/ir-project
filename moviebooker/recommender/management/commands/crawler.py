import csv
import json
import os
import time

import requests as re
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

BASE_PATH = '/Users/omercangokkaya/myworkspace/METU/IR/Project/Implementation/IRProject'
DATA_PATH = os.path.join(BASE_PATH, 'data')

GOODREADS_USER = ''
GOODREADS_PASS = ''


def get_goodreads_description(url):
    content = re.get(url).text
    soup = BeautifulSoup(content, 'html.parser')
    try:
        book_title = soup.find('div', {'class': 'infoBoxRowItem'}).text
    except:
        book_title = ''
    try:
        description_divs = soup.find('div', {'id': 'description'})
        book_description = description_divs.find_all('span')[-1].text
    except:
        book_description = ''
    return book_title, book_description


def get_imdb_description(url):
    content = re.get(url).text
    soup = BeautifulSoup(content, 'html.parser')
    title, year = soup.find('div', {'class': 'title_wrapper'}).find('h1').text.split('\xa0')

    storyline = soup.find('div', {'id': 'titleStoryLine'})
    canwrap = storyline.find('div', {'class': 'inline canwrap'})
    try:
        movie_description = canwrap.find('span').text
    except:
        movie_description = ''
    return title, movie_description


def get_imdb_with_selenium(genre):
    driver = webdriver.Firefox()
    url = f"https://www.imdb.com/search/title/?title_type=movie&genres={genre}&sort=num_votes,desc&explore=title_type,genres"
    driver.get(url)

    all_urls = []
    for i in range(0, 40):
        try:
            print(driver.current_url.split('start')[1].split('&')[0][1:])
        except:
            print(0)
        h3s = driver.find_elements_by_class_name('lister-item-header')
        urls = [h3.find_element_by_tag_name('a').get_attribute('href') for h3 in h3s]
        all_urls.extend(urls)
        nexts = driver.find_elements_by_class_name('lister-page-next')
        next_button = nexts[0]
        next_button.click()
    with open(os.path.join(DATA_PATH, f"movie_urls_{genre}.json"), 'w') as f:
        json.dump(all_urls, f)

    with open(os.path.join(DATA_PATH, f"movie_urls_{genre}.json"), 'r') as f:
        all_urls = json.load(f)

    title_description_list = []
    for ind, url in enumerate(all_urls):
        print(ind)
        title, description = get_imdb_description(url)
        title = title.strip()
        description = description.strip()
        if title and description:
            title_description_list.append([url, title, description])
    with open(os.path.join(DATA_PATH, f"movie_descriptions_{genre}.json"), 'w') as f:
        json.dump(title_description_list, f)


def get_goodreads_with_selenium(genres):
    driver = webdriver.Firefox()
    driver.get('https://www.goodreads.com/user/sign_in')
    user_mail = driver.find_element_by_id('user_email')
    user_mail.send_keys(GOODREADS_USER)
    user_pass = driver.find_element_by_id('user_password')
    user_pass.send_keys(GOODREADS_PASS)
    user_pass.send_keys()
    user_pass.send_keys(Keys.ENTER)
    time.sleep(5)
    all_urls = set()

    for genre in genres:
        if len(all_urls) >= 1999:
            all_urls = list(all_urls)[:2000]
            break
        url = f"https://www.goodreads.com/shelf/show/{genre}"
        driver.get(url)

        for i in range(0, 40):
            if len(all_urls) > 2000:
                all_urls = list(all_urls)[:2000]
                break
            print(i)
            h3s = driver.find_elements_by_class_name('bookTitle')
            for h3 in h3s:
                all_urls.add(h3.get_attribute('href'))
            print(len(all_urls))
            nexts = driver.find_elements_by_class_name('next_page')
            if not nexts:
                break
            next_button = nexts[0]
            next_button.click()
    with open(os.path.join(DATA_PATH, f"book_urls_{genre}.json"), 'w') as f:
        json.dump(all_urls, f)

    title_description_list = []
    for ind, url in enumerate(all_urls):
        print(ind)
        title, description = get_goodreads_description(url)

        title = title.strip()
        description = description.strip()

        if title and description:
            title_description_list.append([url, title, description])
    with open(os.path.join(DATA_PATH, f"book_descriptions_{genre}.json"), 'w') as f:
        json.dump(title_description_list, f)


def set_ids(read_path, write_path, start_index):
    output = []
    with open(read_path, 'r') as read_file:
        data = json.load(read_file)
        for url, title, desc in data:
            output.append({
                'id': start_index,
                'url': url,
                'title': title,
                'desc': desc
            })
            start_index += 1
    with open(write_path, 'w') as write_file:
        json.dump(output, write_file)

    return start_index


if __name__ == '__main__':
    fetch_data = False
    pre_process_data = True

    if fetch_data:
        for genre in ['sport', 'sci-fi', 'Horror']:
            get_imdb_with_selenium(genre)

        for genre in [['science-fiction', 'fiction--sf', 'sci-fi'],
                      ['horror', 'horror-fiction', 'classic-horror', 'horror-classics'],
                      ['sport', 'sports-books', 'sports']]:
            get_goodreads_with_selenium(genre)

    if pre_process_data:
        start_index = 1
        for genre in ['sport', 'sci-fi', 'Horror']:
            start_index = set_ids(os.path.join(DATA_PATH, f"movie_descriptions_{genre}.json"), os.path.join(DATA_PATH, f"movie_descriptions_indexed_{genre}.json"), start_index)

        for genre in 'sci-fi', 'horror-classics', 'sports':
            start_index = set_ids(os.path.join(DATA_PATH, f"book_descriptions_{genre}.json"), os.path.join(DATA_PATH, f"book_descriptions_indexed_{genre}.json"), start_index)
