# Generated by Django 3.2.4 on 2021-06-29 20:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=255)),
                ('url', models.CharField(max_length=255, null=True)),
                ('desc', models.TextField(null=True)),
                ('image_url', models.CharField(max_length=255, null=True)),
            ],
        ),
    ]
