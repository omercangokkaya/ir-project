from random import shuffle

from django.shortcuts import render

# Create your views here.
from django.views import View

from recommender.models import Book, Movie
from recommender.utils import get_recommended_movies_by_book


class BookView(View):
    url_name = 'Book View'

    def get(self, request):
        book = Book.objects.get(id=6247)
        random_books = [book] + list(Book.objects.filter(image_url__isnull=False).order_by("?")[:23])
        shuffle(random_books)

        context = {
            'books': Book.objects.all(),
            'random_books': random_books
        }

        return render(request, 'recommender/book.html', context=context)


class RecommendedMovieView(View):
    url_name = 'Recommended Movie View'

    def get(self, request, book_id):
        book = Book.objects.get(id=book_id)
        # Call movie_recommender_function
        retrieved_movies = get_recommended_movies_by_book(book)

        retrieved_movies = list(enumerate(retrieved_movies, start=1))
        context = {
            'book': book,
            'movies': retrieved_movies
        }
        for r, m in retrieved_movies:
            print(f'{book.id};{book.title};{m.id};{m.title}')

        return render(request, 'recommender/movie.html', context=context)


    def post(self, request, book_id):
        relevant_movie_ids = set()
        relevant_movie_ids.add(request.POST['most_relevant_movies_1'])
        relevant_movie_ids.add(request.POST['most_relevant_movies_2'])
        relevant_movie_ids.add(request.POST['most_relevant_movies_3'])

        book = Book.objects.get(id=book_id)
        feedback_movies = Movie.objects.filter(id__in=relevant_movie_ids)
        # Call movie_recommender_function with feedback
        retrieved_movies = get_recommended_movies_by_book(book, feedback_movies=feedback_movies)

        retrieved_movies = list(enumerate(retrieved_movies, start=1))
        context = {
            'book': book,
            'movies': retrieved_movies
        }


        return render(request, 'recommender/movie.html', context=context)
