import json
import math
import string

from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize

from recommender.models import Book, Movie


def preprocess(text):
    ############3************************ABBREVIATIONS EXPAND EDILMEDI; EKSIK KALDI. ########################

    text = text.split()
    allwords = []
    for i in range(0, len(text)):
        tokens = word_tokenize(text[i])
        tokens = [w.lower() for w in tokens]
        table = str.maketrans('', '', string.punctuation)
        stripped = [w.translate(table) for w in tokens]
        words = [word for word in stripped if word.isalpha()]
        stop_words = set(stopwords.words("english"))
        stemmer = SnowballStemmer("english")
        words = [stemmer.stem(w) for w in words if not w in stop_words]
        allwords += words

    return allwords


def okapi_BM25(matched_movies, tf_idf_list, avdl, n, movie_list):  # n is number of docs to retrieve
    retrieve = [0] * n  # movie_no's of top n relevant documents
    ret_scores = [0] * n  # okapi scores of top n docs
    k = 1.5
    b = 0.75
    for i in range(0, len(matched_movies)):
        movie_no = matched_movies[i]
        dl = Movie.objects.get(id=movie_no).doc_len  # movie_list comes from the main scope
        # print(dl)
        # print(type(dl))

        score = 0
        for tpl in tf_idf_list[i]:  # iterate through (tf,logidf) tuples of that movies
            tf = tpl[0]
            logidf = tpl[1]
            score += logidf * (k + 1) * tf / (k * ((1 - b) + b * (dl / avdl)) + tf)

        minim = min(ret_scores)
        if score > minim:
            retrieve[ret_scores.index(minim)] = movie_no
            ret_scores[ret_scores.index(minim)] = score
    score_list = []
    for i in range(n):
        score_list.append([retrieve[i], ret_scores[i]])
    score_list.sort(key=lambda x: x[1], reverse=True)
    return [i[0] for i in score_list]


def taat_processor(q_words, q_counts, vocabulary, inverted_index, avg_doc_len, movie_list, number_of_movies_to_be_retrieved=10):
    matching_movies = []
    tf_idf_movies = []
    N = len(vocabulary)

    for word in q_words:
        if word in inverted_index.keys():
            df = len(inverted_index[word])
            logidf = round(math.log10(N / df), 3)
            idx_line = inverted_index[word]  # 2_dim list of [movie_no,count] lists
        else:
            continue
        for movie in idx_line:
            movie_no = movie[0]
            tf = movie[1]
            if movie_no not in matching_movies:
                matching_movies.append(movie_no)
                tf_idf_movies.append([(tf, logidf)])
            else:
                tf_idf_movies[matching_movies.index(movie_no)].append((tf, logidf))  # [[(tf1,logidf1),(tf2,logidf2),...],...] --> each inner list corresponds to the movie at the same index at matching_movies***


    n = number_of_movies_to_be_retrieved  # number oF docs to retrieve**************

    top_n = okapi_BM25(matching_movies, tf_idf_movies, avg_doc_len, n, movie_list)  # MOVIE_NO's of top n relevant docs. avg_doc_len comes from the main scope
    return top_n


def retrieve_movies(book, movie_list, vocabulary, inverted_index, avg_doc_len, beta, feedback_movies=[], number_of_movies_to_be_retrieved=10):  # beta is the coefficient of positive feedback (used id relevance flag is 1)
    book_descr = preprocess(book.desc)
    # print(book_descr)
    query_words = []  # query word vector
    word_counts = []  # query count vector
    for i in range(0, len(book_descr)):
        word = book_descr[i]
        if word in query_words:
            word_counts[query_words.index(word)] += 1
            continue
        else:
            query_words.append(word)
            word_counts.append(1)

    # movie_file = open('movies.json', 'r')
    # raw_movie_list = json.load(movie_file)
    # movie_file.flush()
    # movie_file.close()

    for mov in feedback_movies:
        mov_descr = preprocess(mov.desc)
        for i in range(0, len(mov_descr)):
            word = mov_descr[i]
            if word in query_words:
                word_counts[query_words.index(word)] += beta
                continue
            else:
                query_words.append(word)
                word_counts.append(beta)

    movie_nos = taat_processor(query_words, word_counts, vocabulary, inverted_index, avg_doc_len, movie_list, number_of_movies_to_be_retrieved)

    ret_list = []
    for no in movie_nos:
        # print(movie_list[no][0])
        movie = Movie.objects.get(id=no)
        ret_list.append(movie)

    return ret_list


def get_recommended_movies_by_book(book, feedback_movies=[], number_of_movies_to_be_retrieved=10):

    idx_file = open(f'data/processed_data/invertedindex_{book.category}', 'r')
    inverted_index = eval(idx_file.read())  # index dictionary
    idx_file.flush()
    idx_file.close()

    vocab_file = open(f'data/processed_data/vocab_{book.category}', 'r')
    vocabulary = eval(vocab_file.read())  # vocabulary list
    vocab_file.flush()
    vocab_file.close()

    movielist_file = open(f'data/processed_data/movielist_{book.category}', 'r')
    movie_list = eval(movielist_file.read())  # list of (moviename,doc_len) tuples (in the original order)
    movielist_file.flush()
    movielist_file.close()

    avg_doc_len = 0
    for elm in movie_list:
        avg_doc_len += elm[1]
    avg_doc_len = avg_doc_len / len(movie_list)

    # retrieve_movies(book_name,1,0.5)
    retrieved_movies = retrieve_movies(book=book,
                                       movie_list=movie_list,
                                       vocabulary=vocabulary,
                                       inverted_index=inverted_index,
                                       avg_doc_len=avg_doc_len,
                                       beta=0.2,
                                       feedback_movies=feedback_movies,
                                       number_of_movies_to_be_retrieved=number_of_movies_to_be_retrieved)  # 0 is relevance feedback flag (closed for now)

    return retrieved_movies

