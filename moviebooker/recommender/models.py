from django.db import models

# Create your models here.
from django.db.models import AutoField


class Book(models.Model):
    id = AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255, null=True)
    desc = models.TextField(null=True)
    image_url = models.CharField(max_length=255, null=True)
    category = models.CharField(max_length=100, null=True)


class Movie(models.Model):
    id = AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255, null=True)
    desc = models.TextField(null=True)
    image_url = models.CharField(max_length=255, null=True)
    category = models.CharField(max_length=100, null=True)
    doc_len = models.IntegerField(null=True)