# Authentication URLS
from django.conf.urls import url
from django.urls import path

from recommender import views

urlpatterns = [

    path('', views.BookView.as_view(),
         name=views.BookView.url_name),
    path('recommender_movies/<int:book_id>/', views.RecommendedMovieView.as_view(),
         name=views.RecommendedMovieView.url_name),
]
